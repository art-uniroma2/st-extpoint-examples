URI Generator Code Example
==========================
This project defines an example implementation of the extension point [URIGenerator][1] mimicking the behavior
of the [Native Template-based URI Generator][2].

Customizations required to start a new project
----------------------------------------------
`pom.xml`

Locate the following elements at the beginning of the file

    <groupId>it.uniroma2.art.semanticturkey.examples</groupId>
    <artifactId>st-urigen-example</artifactId>
    <version>0.0.1-SNAPSHOT</version>

    
You should change the `groupId`, `artifactId` and `version` to meet your requirements.

**Note:** you should rename the root folder of the project to match the chosen `artifactId`.

[1]: http://semanticturkey.uniroma2.it/documentation/adv_user/uri_generator.jsf
[2]: http://semanticturkey.uniroma2.it/documentation/adv_user/uri_generator.jsf#native_template-based_uri_generator

`src/main/java`

There are three classes:
* `ExampleTemplateBasedURIGenerator`: the actual implementation of the extension point
* `ExampleTemplateBasedURIGeneratorFactory`: the factory producing instances of the implementation class
* `ExampleTemplateBasedURIGeneratorConfiguration`: the configuration class describing the parameters

It is possible to rename the classes (together with the package name) freely.

`src/main/resources/META-INF/spring/st-urigen-example.xml`

You can rename this file freely. Moreover, locate the element `bean`

    <bean
		   class="it.uniroma2.art.semanticturkey.examples.expoints.urigen.ExampleTemplateBasedURIGeneratorFactory" />

Update the value of the attribute `class` to match the refactored names.