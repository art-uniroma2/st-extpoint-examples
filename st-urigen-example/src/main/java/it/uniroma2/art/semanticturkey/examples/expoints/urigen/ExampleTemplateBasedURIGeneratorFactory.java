package it.uniroma2.art.semanticturkey.examples.expoints.urigen;

import java.util.Arrays;
import java.util.Collection;

import it.uniroma2.art.semanticturkey.examples.expoints.urigen.conf.ExampleTemplateBasedURIGeneratorConfiguration;
import it.uniroma2.art.semanticturkey.plugin.PluginFactory;
import it.uniroma2.art.semanticturkey.plugin.configuration.UnloadablePluginConfigurationException;
import it.uniroma2.art.semanticturkey.plugin.configuration.UnsupportedPluginConfigurationException;
import it.uniroma2.art.semanticturkey.properties.STProperties;

public class ExampleTemplateBasedURIGeneratorFactory implements
		PluginFactory<ExampleTemplateBasedURIGeneratorConfiguration, STProperties, STProperties, STProperties, STProperties> {

	@Override
	public String getID() {
		return this.getClass().getName();
	}

	@Override
	public Collection<STProperties> getPluginConfigurations() {
		return Arrays.<STProperties>asList(new ExampleTemplateBasedURIGeneratorConfiguration());
	}

	@Override
	public ExampleTemplateBasedURIGeneratorConfiguration createDefaultPluginConfiguration() {
		return new ExampleTemplateBasedURIGeneratorConfiguration();
	}

	@Override
	public ExampleTemplateBasedURIGeneratorConfiguration createPluginConfiguration(String configType)
			throws UnsupportedPluginConfigurationException, UnloadablePluginConfigurationException,
			ClassNotFoundException {
		Class<?> clazz = Class.forName(configType);

		if (!ExampleTemplateBasedURIGeneratorConfiguration.class.isAssignableFrom(clazz)) {
			throw new UnsupportedPluginConfigurationException();
		}

		try {
			return (ExampleTemplateBasedURIGeneratorConfiguration) clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new UnloadablePluginConfigurationException(e);
		}
	}

	@Override
	public ExampleTemplateBasedURIGenerator createInstance(STProperties config) {
		return new ExampleTemplateBasedURIGenerator((ExampleTemplateBasedURIGeneratorConfiguration) config);
	}

}
