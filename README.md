Semantic Turkey Extension Point Code Examples
=============================================
The modules of this project provide example implementations of the extension point defined by the knowledge
management and acquisition platform [Semantic Turkey](http://semanticturkey.uniroma2.it/).

The MASTER branch of this project refers to the version 6.0 of Semantic Turkey (current stable version).

Current example projects:

* **st-urigen-example**: an example implementation of the extension point [URIGenerator][1] mimicking the
    behavior of the [Native Template-based URI Generator][2]
* **st-rdflifter-example**: an example implementation of the extension point [RDFLifter][3] mimicking the
    behavior of the [RDF Deserializing Lifter][4]
* **st-reformattingexporter-example**: an example implementation of the extension point [ReformattingExporter][5] mimicking the
    behavior of the [RDF Serializing Exporter][6]

Those projects can be used as a starting point for the development of new implementations of a given extension
point. For this reason, the README of each project provides detailed instructions on which modifications are 
required to start a new project.

[1]: http://semanticturkey.uniroma2.it/documentation/adv_user/uri_generator.jsf
[2]: http://semanticturkey.uniroma2.it/documentation/adv_user/uri_generator.jsf#native_template-based_uri_generator
[3]: http://semanticturkey.uniroma2.it/doc/sys/rdf_lifter.jsf
[4]: http://semanticturkey.uniroma2.it/doc/sys/rdf_lifter.jsf#rdf_deserializing_lifter
[5]: http://semanticturkey.uniroma2.it/doc/sys/reformatting_exporter.jsf
[6]: http://semanticturkey.uniroma2.it/doc/sys/reformatting_exporter.jsf#rdf_serializing_exporter
