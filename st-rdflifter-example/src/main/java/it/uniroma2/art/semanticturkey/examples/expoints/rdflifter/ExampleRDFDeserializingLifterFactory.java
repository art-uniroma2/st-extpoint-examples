package it.uniroma2.art.semanticturkey.examples.expoints.rdflifter;

import static java.util.stream.Collectors.toList;

import java.util.List;

import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.commons.io.FormatCapabilityProvider;
import it.uniroma2.art.semanticturkey.resources.DataFormat;
import it.uniroma2.art.semanticturkey.utilities.RDF4JUtilities;


public class ExampleRDFDeserializingLifterFactory
		implements NonConfigurableExtensionFactory<ExampleRDFDeserializingLifter>, FormatCapabilityProvider {

	@Override
	public String getName() {
		return "Example RDF Deserializing Lifter";
	}

	@Override
	public String getDescription() {
		return "An RDF Lifter that deserializes RDF data according to a concrete RDF syntax";
	}

	@Override
	public ExampleRDFDeserializingLifter createInstance() {
		return new ExampleRDFDeserializingLifter();
	}

	@Override
	public List<DataFormat> getFormats() {
		return RDF4JUtilities.getInputFormats().stream().map(DataFormat::valueOf).collect(toList());
	}

}
