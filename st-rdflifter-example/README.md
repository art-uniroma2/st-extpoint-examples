RDF Lifter Code Example
==========================
This project defines an example implementation of the extension point [RDFLifter][1] mimicking the behavior
of the [RDF Deserializing Lifter][2].

Customizations required to start a new project
----------------------------------------------
`pom.xml`

Locate the following elements at the beginning of the file

    <groupId>it.uniroma2.art.semanticturkey.examples</groupId>
    <artifactId>st-rdflifter-example</artifactId>
    <version>0.0.1-SNAPSHOT</version>

    
You should change the `groupId`, `artifactId` and `version` to meet your requirements.

**Note:** you should rename the root folder of the project to match the chosen `artifactId`.

[1]: http://semanticturkey.uniroma2.it/doc/sys/rdf_lifter.jsf
[2]: http://semanticturkey.uniroma2.it/doc/sys/rdf_lifter.jsf#rdf_deserializing_lifter

`src/main/java`

There are two classes:
* `ExampleRDFDeserializingLifter`: the actual implementation of the extension point
* `ExampleRDFDeserializingLifterFactory`: the factory producing instances of the implementation class

It is possible to rename the classes (together with the package name) freely.

`src/main/resources/META-INF/spring/st-rdflifter-example.xml`

You can rename this file freely. Moreover, locate the element `bean`

    <bean
		   class="it.uniroma2.art.semanticturkey.examples.expoints.rdflifter.ExampleRDFDeserializingLifterFactory" />

Update the value of the attribute `class` to match the refactored names.