package it.uniroma2.art.semanticturkey.examples.expoints.reformattingexporter;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.PUScopedConfigurableComponent;
import it.uniroma2.art.semanticturkey.extension.extpts.commons.io.FormatCapabilityProvider;
import it.uniroma2.art.semanticturkey.resources.DataFormat;
import it.uniroma2.art.semanticturkey.utilities.RDF4JUtilities;

public class ExampleRDFSerializingExporterFactory
		implements ConfigurableExtensionFactory<ExampleRDFSerializingExporter, ExampleRDFSerializingExporterConfiguration>,
		PUScopedConfigurableComponent<ExampleRDFSerializingExporterConfiguration>, FormatCapabilityProvider {

	@Override
	public String getName() {
		return "Example RDF Serializing Exporter";
	}

	@Override
	public String getDescription() {
		return "A Reformatting Exporter that serializes RDF data according to a concrete RDF syntax";
	}

	@Override
	public List<DataFormat> getFormats() {
		return RDF4JUtilities.getOutputFormats().stream().map(DataFormat::valueOf).collect(toList());
	}

	@Override
	public ExampleRDFSerializingExporter createInstance(ExampleRDFSerializingExporterConfiguration conf) {
		return new ExampleRDFSerializingExporter(conf);
	}

	@Override
	public Collection<ExampleRDFSerializingExporterConfiguration> getConfigurations() {
		return Arrays.asList(new ExampleRDFSerializingExporterConfiguration());
	}

}
